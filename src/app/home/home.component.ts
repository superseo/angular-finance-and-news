import { Component, OnInit } from '@angular/core';
import { ItemService } from '../item.service';
import { FinancePostDynamic } from '../items/finance/finance.component';
import { NewsPostDynamic } from '../items/news/news.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  items = [];
  types = {
    'news': NewsPostDynamic,
    'finance': FinancePostDynamic
  };
  itemsOrigin = [];
  filterModel;
  filterOptions:Array<any> = ['По умолчанию', 'Сначала новые', 'Сначала старые', 'Сначала финансовые', 'Сначала новости'];

  constructor(
    private itemService: ItemService
  ) { }

  // при инициализации подпишемся на данные из ItemService
  ngOnInit() {
    this.itemService.getItems().then(data => {
      console.log('data', data);
      if (data !== undefined && data.results) {
        this.items = data.results;
        this.itemsOrigin = [...this.items];
      }
    });
  }

  onChange(e) {
    if (e == 0) {
      this.items = [...this.itemsOrigin];
      return;
    }
    this.items.sort((a, b) => {
      return (e == 1) ? <any>new Date(b.date) - <any>new Date(a.date) :
             (e == 2) ? <any>new Date(a.date) - <any>new Date(b.date) :
             (e == 3) ? ((a.type == 'finance') ? -1 : 1) :
             (e == 4) ? ((a.type == 'news') ? -1 : 1) : 0;
    });
  }
}
