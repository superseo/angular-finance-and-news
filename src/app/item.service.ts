import { Injectable } from '@angular/core';
import Data from './data.json';

@Injectable()
export class ItemService {

  // значение по умолчанию
  perPage = 4;
  // constructor(private http: Http) { }

  // геттер элементов
  // http объект возвращает http rxjs стрим, на который нужно подписаться
  getItems() {
    let results = Data;
    return Promise.resolve({results, status: 'OK'});
  }

  // сеттер нового значения perPage
  setPerPage(perPage) {
    this.perPage = perPage;
  }

}
