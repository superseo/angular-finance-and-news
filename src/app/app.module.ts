import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgxdModule } from '@ngxd/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';

import { CategoryComponent } from './category/category.component';
import { SettingsComponent } from './settings/settings.component';
import { ContactsComponent } from './contacts/contacts.component';
import { HomeComponent } from './home/home.component';
import { BasePostComponent } from './items/base/base.component'
import { FinancePostDynamic } from './items/finance/finance.component'
import { NewsPostDynamic } from './items/news/news.component'

import { ItemService } from './item.service';

// роуты и соответствующие им компоненты, попадает в router-outlet
const routes = [
  { path: '', component: HomeComponent },
  { path: 'settings', component: SettingsComponent },
  { path: 'contacts', component: ContactsComponent },
  { path: 'category', component: CategoryComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    CategoryComponent,
    SettingsComponent,
    ContactsComponent,
    HomeComponent,
    BasePostComponent,
    FinancePostDynamic,
    NewsPostDynamic
  ],
  imports: [
    NgxdModule,
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  entryComponents: [
    FinancePostDynamic,
    NewsPostDynamic
  ],
  // глобально объявляем провайдер, для глобальной подписки на изменяемые параметры
  providers: [ItemService],
  bootstrap: [AppComponent]
})
export class AppModule { }
