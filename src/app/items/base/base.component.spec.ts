import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasePostComponent } from './base.component';

describe('BasePostComponent', () => {
  let component: BasePostComponent;
  let fixture: ComponentFixture<BasePostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasePostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasePostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
