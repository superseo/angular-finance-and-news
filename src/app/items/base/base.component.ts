// import { Component, Output, Input, EventEmitter, Type } from '@angular/core';
import { Component, Output, Input, EventEmitter } from '@angular/core';

abstract class PostBase {
  @Input() item;
  @Output() say: EventEmitter<any> = new EventEmitter<any>();
}

@Component({
  selector: 'post',
  template: `
    <ng-container
      *ngxComponentOutlet="this | resolve: cls">
    </ng-container>
  `,
  styleUrls: ['../base/base.component.scss']
})
export class BasePostComponent extends PostBase {
  @Input() cls: BasePostComponent;
  // resolve(cls: BasePostComponent): Type<BasePostComponent>{
  resolve(cls: BasePostComponent) {
    return cls;
  }
}