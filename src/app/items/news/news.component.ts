import { Component } from '@angular/core';
import { BasePostComponent } from "../base/base.component";

@Component({
  selector: 'news',
  templateUrl: '../news/news.component.html',
  styleUrls: ['../news/news.component.scss']
})
export class NewsPostDynamic extends BasePostComponent {
  constructor() {
    super();
  }
}