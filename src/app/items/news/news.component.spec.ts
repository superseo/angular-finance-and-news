import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsPostDynamic } from './news.component';

describe('NewsPostDynamic', () => {
  let component: NewsPostDynamic;
  let fixture: ComponentFixture<NewsPostDynamic>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsPostDynamic ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsPostDynamic);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
