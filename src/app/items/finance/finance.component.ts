import { Component } from '@angular/core';
import { BasePostComponent } from "../base/base.component";

@Component({
  selector: 'finance',
  templateUrl: '../finance/finance.component.html',
  styleUrls: ['../finance/finance.component.scss']
})
export class FinancePostDynamic extends BasePostComponent {
    constructor() {
      super();
    }
}