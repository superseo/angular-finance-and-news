import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinancePostDynamic } from './finance.component';

describe('FinancePostDynamic', () => {
  let component: FinancePostDynamic;
  let fixture: ComponentFixture<FinancePostDynamic>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinancePostDynamic ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinancePostDynamic);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
